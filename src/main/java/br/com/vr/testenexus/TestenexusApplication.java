package br.com.vr.testenexus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestenexusApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestenexusApplication.class, args);
	}

}
